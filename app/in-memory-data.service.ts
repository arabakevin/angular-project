// Imports
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { POKEMONS } from './pokemons/mock-pokemons';

// In fact, Angular can allow to dissimilate an API who will working as
// if we using a distant server!
export class InMemoryDataService implements InMemoryDbService{
  // This method can be simulate a little database based from data file
  // ( here -> mock-pockemons )
  createDb() {
    let pokemons = POKEMONS;
    return { pokemons };
  }
}
