import { Component } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';

@Component({
	selector: 'page-404',
	template: `
    <h1 class='center'>PAGE 404</h1>
		<div class= 'row'>
      <div class='center'>
        <div class="col s4">
          <img src="app/assets/pokemons_pictures/001.png">
        </div>
				<div class="col s4">
					<img src="app/assets/pokemons_pictures/004.png">
				</div>
				<div class="col s4">
					<img src="app/assets/pokemons_pictures/007.png">
				</div>
				<h3 class="flow-text"> We don't found your research </h3>
      </div>
		</div>
		<div class="card-action">
		<a class="waves-effect waves-light btn" (click)="goBack()" >Retour</a>
  `
})
export class PageNotFoundComponent {
	constructor(private route: ActivatedRoute, private router: Router) {}
	// Method to redirect the user to the main page of the application.
	goBack(): void {
		this.router.navigate(['/pokemon/all']);
	}
}
