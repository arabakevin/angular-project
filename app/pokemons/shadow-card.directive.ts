import { Directive, ElementRef, Renderer, HostListener } from '@angular/core';

// Guidelines: We need them to use the @Directive annotation.
// ElementRef: This object represents the element of the DOM on which we apply our directive, we have direct access to it as parameter of the constructor.
// Renderer: This object allows us to change the style of the DOM element on which the directive applies.

@Directive({ selector: '[pkmn-shadow-card]' })
export class ShadowCardDirective {
  constructor(private el: ElementRef, private renderer: Renderer) {
     this.setBorder('#f5f5f5');
     this.setHeight('180px');
   }

 // @HostListener. This annotation makes it possible to link a method of our directive to a given event.
 // We will therefore create two new methods in our directive, which will be called respectively
 // when the user's cursor enters an element, and when it comes out:
 // these are the mouseenter and mouseleave events.

   @HostListener('mouseenter') onMouseEnter() {
     this.setBorder('#009688');
   }

   @HostListener('mouseleave') onMouseLeave() {
     this.setBorder('#f5f5f5');
   }

   private setBorder(color: string) {
     let style = 'solid 4px ' + color;
     this.renderer.setElementStyle(this.el.nativeElement, 'border', style);
   }

   private setHeight(height: string) {
     this.renderer.setElementStyle(this.el.nativeElement, 'height', height);
   }

}
