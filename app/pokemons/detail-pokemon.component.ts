// Base import
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Title } from '@angular/platform-browser';
// Data import
import { POKEMONS } from './mock-pokemons';

// Model import
import { Pokemon } from './pokemon';

// Service import
import { PokemonsService } from './pokemons.service'; // We import here the service PokemonsService.
@Component({
  selector: 'detail-pokemon',
  template: `
    <div *ngIf="pokemon" class="row">
   <div class="col s12 m8 offset-m2">
   <h2 class="header center">{{ pokemon.name }}</h2>
   <div class="card horizontal hoverable">
     <div class="card-image">
       <img [src]="pokemon.picture">
     </div>
     <div class="card-stacked">
       <div class="card-content">
         <table class="bordered striped">
           <tbody>
             <tr>
               <td>Name</td>
               <td><strong>{{ pokemon.name }}</strong></td>
             </tr>
             <tr>
               <td>Life point</td>
               <td><strong>{{ pokemon.hp }}</strong></td>
             </tr>
             <tr>
               <td>Damage point</td>
               <td><strong>{{ pokemon.point_of_damage }}</strong></td>
             </tr>
             <tr>
               <td>Types</td>
               <td>
                 <span *ngFor="let type of pokemon.types" class="{{ type | pokemonTypeColor }}">{{ type }}</span>
               </td>
             </tr>
             <tr>
               <td>Création date</td>
               <td><em>{{ pokemon.created_date | date:"dd/MM/yyyy" }}</em></td>
             </tr>
           </tbody>
         </table>
       </div>
       <div class="card-action">
        <a class="waves-effect waves-light btn" (click)="goBack()" >Back</a>
        <a class="waves-effect waves-light btn" (click)="goEdit(pokemon)" >Edit</a>
       </div>
     </div>
   </div>
   </div>
   </div>
   <h4 *ngIf='!pokemon' class="center">
    <pkmn-loader></pkmn-loader>
   </h4>
  `
})

export class DetailPokemonComponent implements OnInit {

  // declaration of a table of the list of pokémons of our application
  // pokemons: Pokemon[] = null;
  pokemon: Pokemon = null; // pokémon to display in the template

	constructor(private route: ActivatedRoute,
              private router: Router,
              private PokemonsService : PokemonsService,
              private titleService: Title) {}
      // we inject 'route' to retrieve the parameters of the url,
      // and 'router' to redirect the user.
      // PokemonsService is a service to implement in the constructor

  ngOnInit(): void {
    this.route.params.forEach((params: Params) => {
      let id = +params['id'];
      this.PokemonsService.getPokemon(id).then(pokemon => this.pokemon = pokemon);
    });
  }

  // Method to redirect the user to the main page of the application.
  goBack(): void {
	this.router.navigate(['/pokemon/all']);
  this.modifyTitle("all pokemons");
  }

  goEdit(pokemon: Pokemon): void {
    // Method to redirect the user to the main page of the application.
    let link = ['/pokemon/edit', pokemon.id];
    this.router.navigate(link);
    this.modifyTitle("edit pokemon");
  }

  // Modify Title. Here we change the title of the tab.
  public modifyTitle(newTitle: string) {
    this.titleService.setTitle(newTitle);
  }
}
