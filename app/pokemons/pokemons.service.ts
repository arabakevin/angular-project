import { Injectable } from '@angular/core';
import { POKEMONS } from './mock-pokemons';
import { Pokemon } from './pokemon';

// Importation Http module and Promise
// importation of Hearders to use an put method
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';

// We import here the package of Notification message package.
import { Ng2IzitoastService } from 'ng2-izitoast';

// @Injectable id for create a service.
//They indicate what we can have dependancies too
@Injectable()
export class PokemonsService {

  // constructor ...
  constructor(private http: Http, private iziToast: Ng2IzitoastService) { }

  // get for all pokemon existing
  getPokemons(): Promise<Pokemon[]> {
    return this.http.get('api/pokemons')
       .toPromise()
       .then(response => response.json().data as Pokemon[])
       .catch(this.handleError);
  }

  // get a pokemom
  getPokemon(id: number): Promise<Pokemon> {
    const url = 'api/pokemons/' + id;

    return this.http.get(url)
      .toPromise()
      .then(response => response.json().data as Pokemon)
      .catch(this.handleError);
  }

  update(pokemon: Pokemon): Promise<Pokemon>{
    // we update a particular pokemon to know which pokemon by the api must be update
    const url = `api/pokemons/${pokemon.id}`;
    let headers = new Headers ({'Content-Type': 'application/json'});
    return this.http
               // In a put method, we have 3 parts (url, body request and headers)
               // stringify is a method who convert an JS object in a character chain
               .put(url, JSON.stringify(pokemon), headers)
               .toPromise()
               .then(() => this.succes_response(pokemon))
               .catch(error => {
                 this.iziToast.error({
                   title: `Edit ${pokemon.name}`,
                   position: 'topCenter',
                   transitionIn: 'flipInX',
                   message: 'An error happened !',
                 });
                 this.handleError(error);
               });
  }

  // return the type of pokemons
  getPokemonTypes(): Array<string>{
    return [
      'Plant', 'Fire', 'Water', 'Insect', 'Normal', 'Electrik',
      'Poison', 'Fairy', 'Flight', 'Combat', 'Psy'
    ];
  }

  private succes_response (pokemon: Pokemon): Pokemon{
    this.iziToast.success({
      title: `Edit ${pokemon.name}`,
      position: 'topCenter',
      transitionIn: 'flipInX',
      message: `the Pokemon ${pokemon.name} has been properly updated!`,
    });
    return pokemon;
  }

  private handleError(error: any): Promise<any> {
    console.log('Error  :', error ); // we display the error in the console
    return Promise.reject(error.message || error);
  }
}
