import { Component, OnInit } from '@angular/core';
// Importation of router
import { Router } from '@angular/router';
// Importation of Service in angular
import { PokemonSearchService } from './pokemon-search.service';
// Importation of Pokemon model
import { Pokemon } from './pokemon';

// Importation rxjs
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';


@Component({
  selector: 'pokemon-search',
  templateUrl: 'app/pokemons/pokemon-search.component.html',
  styleUrls: ['app/pokemons/pokemon-search.component.css'],
  providers: [PokemonSearchService],
})
export class PokemonSearchComponent implements OnInit {

  private searchTerms = new Subject<string>(); // component input property
  private pokemons: Observable<Pokemon[]>; // possible types of a pokemon

  constructor(
    private pokemonSearchService: PokemonSearchService, // Here we initalize the service.
    private router: Router) { }

  search(term: string): void {
    // we use next because searchTerms is not an array
    this.searchTerms.next(term);
  }

  ngOnInit(): void {
    this.pokemons = this.searchTerms
      // we wait 300 ms before send a other call
      .debounceTime(300)
      // we verifiy if the term of search has not changed
      .distinctUntilChanged()
      // we return the result if we have and after the conditions.
      .switchMap(term => term
        // ... then we call the method of the service
        ? this.pokemonSearchService.search(term)
        // ... else we can return an empty Observable
        : Observable.of<Pokemon[]>([]))
      .catch(error => {
        console.log(error);
        return Observable.of<Pokemon[]>([]); // if we have an error we return an empty array
      });
  }

  gotoDetail(pokemon: Pokemon): void {
    let link = ['/pokemon', pokemon.id];
    this.router.navigate(link);
  }
}
