//Imports
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Pokemon } from './pokemon';
import { PokemonsService } from './pokemons.service';

@Component({
  selector: 'edit-pokemon',
  // {{ pokemon?.name }} ==> display the name of pokemon of this last existing.
  template: `
    <h2 class="header center">Edit {{ pokemon?.name }}</h2>
    <pokemon-form [pokemon]="pokemon"></pokemon-form>
  `,
})
export class EditPokemonComponent implements OnInit {

  pokemon: Pokemon = null;

  constructor(
    private route: ActivatedRoute,
    private PokemonsService: PokemonsService) {}

  ngOnInit(): void {
    this.route.params.forEach((params: Params) => {
      let id = +params['id'];
      this.PokemonsService.getPokemon(id).then(pokemon => this.pokemon = pokemon);
    });
  }

}
