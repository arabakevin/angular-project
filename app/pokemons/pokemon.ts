export class Pokemon {
id: number;
hp: number;
point_of_damage: number;
name: string;
picture: string;
types: Array<string>;
created_date: Date;
}
