import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Import the differents components
import { ListPokemonComponent }    from './list-pokemon.component';
import { DetailPokemonComponent }  from './detail-pokemon.component';
import { EditPokemonComponent } from './edit-pokemon.component';

import { AuthGuard } from '../auth-guard.service'; // Le guard de l'authentification

import { Title }  from '@angular/platform-browser'; //  'we import the service Title'

// Definition routes
const pokemonsRoutes: Routes = [
	{
		path: 'pokemon',
		canActivate: [AuthGuard], //here we can have use a guard (CanActivate)
		children: [
			{ path: 'all', component: ListPokemonComponent },
			{ path: 'edit/:id', component: EditPokemonComponent },
			{ path: ':id', component: DetailPokemonComponent }
		]
	}
];

@NgModule({
	imports: [
		// the other module is for any imported modules.
		RouterModule.forChild(pokemonsRoutes)
	],
	exports: [
		RouterModule
	]
})
export class PokemonRoutingModule { }
