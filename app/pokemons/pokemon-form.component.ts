import { Component, Input, OnInit } from '@angular/core';

// Importation of router
import { Router } from '@angular/router';

// Importation of Service in angular
import { PokemonsService } from './pokemons.service';

// Importation of Pokemon model
import { Pokemon } from './pokemon';

@Component({
  selector: 'pokemon-form',
  templateUrl: 'app/pokemons/pokemon-form.component.html',
  styleUrls: ['app/pokemons/pokemon-form.component.css'],
  providers: [PokemonsService],
})
export class PokemonFormComponent implements OnInit {

  @Input() pokemon: Pokemon; // component input property
  types: Array<string>; // possible types of a pokemon : 'Water', 'Fire', etc ...

  constructor(
    private pokemonsService: PokemonsService, // Here we initalize the service.
    private router: Router) { }

  ngOnInit() {
    // Initializing the types property
    this.types = this.pokemonsService.getPokemonTypes();
  }
  // We determine if the type take in params belongs or not at pokemin in edition mode.
  hasType(type: string): boolean {
    //var beasts = ['ant', 'bison', 'camel', 'duck', 'bison'];
    //console.log(beasts.indexOf('bison'));
    // expected output: 1
    let index = this.pokemon.types.indexOf(type);
    // The operator "~" is a shortcut of JavaScript who allow to verify than a variable is more than -1.
    if (~index) return true;
    return false;
  }

  // Method called when the user add or remove one type in edition mode
  selectType($event: any, type: string): void {
    // target Get the element that triggered a specific event
    console.log($event);
    let checked = $event.target.checked;

    if ( checked ) {
      this.pokemon.types.push(type);
    } else {
      let index = this.pokemon.types.indexOf(type);
      // "~" is a shortcut of JavaScript who allow to verify than a variable is more than -1.
      if (~index) {
        // splice add element in the array
        this.pokemon.types.splice(index, 1);
      }
    }
  }

  // Method called when we must verify if the pokemon has more 3 types
  isTypeLength(): boolean {
    if (this.pokemon.types.length >= 3){
      return true
    }else{
      return false
    }
  }

  // The method is called when the form is submitted
  onSubmit(): void {
    console.log("Submit form !");
    this.pokemonsService.update(this.pokemon)
    .then(() => {
      let link = ['/pokemon', this.pokemon.id];
      this.router.navigate(link);
    });
  }

  goBack(pokemon: Pokemon): void {
    // Method to redirect the user to the detail of the pokemon advise in params.
    let link = ['/pokemon/', pokemon.id];
    this.router.navigate(link);
  }
}
