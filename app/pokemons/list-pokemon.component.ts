// Imports
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// Data import
import { POKEMONS } from './mock-pokemons';

// Model import
import { Pokemon } from './pokemon';

// Service Import
import {PokemonsService} from './pokemons.service';

@Component({
	selector: 'list-pokemon',
	template: `
    <h1 class='center'>Pokémons</h1>
      <div class='container'>
        <div class="row">
				<pokemon-search></pokemon-search>
        <div *ngFor='let pokemon of pokemons' class="col s6 m4">
          <div class="card horizontal" (click)="selectPokemon(pokemon)" pkmn-shadow-card>
            <div class="card-image">
              <img [src]="pokemon.picture">
            </div>
            <div class="card-stacked">
              <div class="card-content">
                <p>{{ pokemon.name }}</p>
                <p><small>{{ pokemon.created | date:"dd/MM/yyyy" }}</small></p>
                <span *ngFor='let type of pokemon.types' class="{{ type | pokemonTypeColor }}">{{ type }}</span>
              </div>
            </div>
          </div>
        </div>
        </div>
      </div>
  `
})
export class ListPokemonComponent implements OnInit {

	pokemons: Pokemon[] = null;

	// Inject a router into the component
	// Inject a service into the contructor as a second parameter
	constructor(private router: Router,
							private PokemonsService : PokemonsService) { }

	ngOnInit(): void {
		this.getPokemonsList();
	}

	getPokemonsList() : void {
		this.PokemonsService.getPokemons().then(pokemons => this.pokemons = pokemons);
	}

	// Use the router in the selectPokemon method to redirect the user to the page that details a pokemon
	selectPokemon(pokemon: Pokemon): void {
		console.log('or have selected ' + pokemon.name);
    console.log('The pokemon selected has for id' + pokemon.id)
		let link = ['/pokemon', pokemon.id];
		this.router.navigate(link);
	}

}
