import { Pipe, PipeTransform } from '@angular/core';
import { Pokemon } from './pokemon';

/*
 * Display the color correspondant at type of pokemon.
 * Take a argument and this one is the type of pokemon.
 * Example of use:
 *   {{ pokemon.type | pokemonTypeColor }}
*/

@Pipe({name: 'pokemonTypeColor'})
export class PokemonTypeColorPipe implements PipeTransform {
  transform(type: string): string {
    let color: string;

    switch(type){
      case 'Fire':
        color = 'red lighten-1';
        break;
      case 'Water':
        color = 'blue';
        break;
      case 'Plant':
        color = 'green lighten-1';
        break;
      case 'Insect':
        color = 'brown lighten-1';
        break;
      case 'Normal':
        color = 'grey lighten-3';
        break;
      case 'Flight':
        color = 'blue lighten-4';
        break;
      case 'Poison':
        color = 'deep-purple accent-1';
        break;
      case 'Fairy':
        color = 'pink lighten-4';
        break;
      case 'Psy':
        color = 'deep-purple accent-3';
        break;
      case 'Electrik':
        color = 'yellow';
        break;
      case 'Combat':
        color = 'deep-orange';
        break;
      default:
        color = 'grey';
        break;
    }
    return 'chip ' + color;

  }
}
