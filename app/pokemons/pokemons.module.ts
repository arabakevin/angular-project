// Base of import
import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms'; // for the formulat it's necessary to have this import

// Additional import
import { ListPokemonComponent } from './list-pokemon.component';
import { DetailPokemonComponent } from './detail-pokemon.component';
import { ShadowCardDirective } from './shadow-card.directive';
import { PokemonTypeColorPipe } from './pokemon-type-color.pipe';
import { PokemonRoutingModule } from './pokemons-routing.module';
import { PokemonFormComponent } from './pokemon-form.component';
import { EditPokemonComponent } from './edit-pokemon.component';
import { LoaderComponent } from './loader.component';
// pokemons search
import { PokemonSearchComponent } from './pokemon-search.component';
// Service import
import {PokemonsService} from './pokemons.service'
import { AuthGuard } from '../auth-guard.service'; // we import the guard

@NgModule({
  imports:      [ FormsModule, BrowserModule, PokemonRoutingModule],
  declarations: [
    ShadowCardDirective,
    PokemonTypeColorPipe,
    ListPokemonComponent,
    DetailPokemonComponent,
    PokemonFormComponent,
    EditPokemonComponent,
    PokemonSearchComponent,
    LoaderComponent
  ],
  // The providers are the declarations of services who can be declare
  // at the modules level
  providers: [ PokemonsService, AuthGuard ]
})
export class PokemonsModule { }
