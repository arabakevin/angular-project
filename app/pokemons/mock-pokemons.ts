import { Pokemon } from './pokemon';

export const POKEMONS: Pokemon[] = [
	{
		id: 1,
		name: "Bulbizarre",
		hp: 25,
		point_of_damage: 5,
		picture: "app/assets/pokemons_pictures/001.png",
		types: ["Plant", "Poison"],
		created_date: new Date()
	},
	{
		id: 2,
		name: "Salamèche",
		hp: 28,
		point_of_damage: 6,
		picture: "app/assets/pokemons_pictures/004.png",
		types: ["Fire"],
		created_date: new Date()
	},
	{
		id: 3,
		name: "Carapuce",
		hp: 21,
		point_of_damage: 4,
		picture: "app/assets/pokemons_pictures/007.png",
		types: ["Water"],
		created_date: new Date()
	},
	{
		id: 4,
		name: "Aspicot",
		hp: 16,
		point_of_damage: 2,
		picture: "app/assets/pokemons_pictures/013.png",
		types: ["Insect", "Poison"],
		created_date: new Date()
	},
	{
		id: 5,
		name: "Roucool",
		hp: 30,
		point_of_damage: 7,
		picture: "app/assets/pokemons_pictures/016.png",
		types: ["Normal", "Flight"],
		created_date: new Date()
	},
	{
		id: 6,
		name: "Rattata",
		hp: 18,
		point_of_damage: 6,
		picture: "app/assets/pokemons_pictures/019.png",
		types: ["Normal"],
		created_date: new Date()
	},
	{
		id: 7,
		name: "Piafabec",
		hp: 14,
		point_of_damage: 5,
		picture: "app/assets/pokemons_pictures/021.png",
		types: ["Normal", "Flight"],
		created_date: new Date()
	},
	{
		id: 8,
		name: "Abo",
		hp: 16,
		point_of_damage: 4,
		picture: "app/assets/pokemons_pictures/023.png",
		types: ["Poison"],
		created_date: new Date()
	},
	{
		id: 9,
		name: "Pikachu",
		hp: 21,
		point_of_damage: 7,
		picture: "app/assets/pokemons_pictures/025.png",
		types: ["Electrik"],
		created_date: new Date()
	},
	{
		id: 10,
		name: "Sabelette",
		hp: 19,
		point_of_damage: 3,
		picture: "app/assets/pokemons_pictures/027.png",
		types: ["Normal"],
		created_date: new Date()
	},
	{
		id: 11,
		name: "Mélofée",
		hp: 25,
		point_of_damage: 5,
		picture: "app/assets/pokemons_pictures/035.png",
		types: ["Fairy"],
		created_date: new Date()
	},
	{
		id: 12,
		name: "Groupix",
		hp: 17,
		point_of_damage: 8,
		picture: "app/assets/pokemons_pictures/037.png",
		types: ["Fire"],
		created_date: new Date()
	}
];
