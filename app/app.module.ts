// Base of import
import { NgModule }      from '@angular/core';
import { BrowserModule, Title} from '@angular/platform-browser';

// additional import by application
import { AppRoutingModule } from './app-routing.module';
import { PokemonsModule } from './pokemons/pokemons.module';
import { AppComponent }  from './app.component';
import { PageNotFoundComponent } from './page-not-found.component'
import { LoginRoutingModule } from './login-routing.module'
import { LoginComponent } from './login.component'
import './rxjs-extensions';

// Import Ng2IziToastModule. This one is a library for the different notification message
import { Ng2IziToastModule } from 'ng2-izitoast';

 // We import the module
import { HttpModule } from '@angular/http';

// Importation to load and setting the simulate api.
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';

// Global module
@NgModule({
  // here we import the different module
  imports:      [ BrowserModule,
                  HttpModule, //HttpModule to allow communicate with the Http protocol
                  Ng2IziToastModule,
                  InMemoryWebApiModule.forRoot(InMemoryDataService), // The method forRoot take in params the class InMemoryDataService, who initalize the database in memory with the data of the service indicated.
                  PokemonsModule, // The loading order of the modules is very important
                  LoginRoutingModule, // here we found the login module.
                  AppRoutingModule, // for the declaration order of the roads!
                ],
  // here we import the different dependencies
  declarations: [
    AppComponent,
    LoginComponent,
    PageNotFoundComponent
   ],
   providers: [
     Title // we add the service 'Title'
  ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
