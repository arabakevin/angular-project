import { Injectable }     from '@angular/core';
import {
  CanActivate, Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
}                           from '@angular/router';
import { AuthService }      from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  // if canActivate return true we can continue the navigation
  // if canActivate return true we are redirect on the login page

  // ActivatedRouteSnapshot, belongs the future road who will it call.
  // RouterStateSnapshot belongs the future state of routor of application
  // who will pass the guard verification.
	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):boolean {
    console.log("RouterStateSnapshot"+ state.url);
		let url : string = state.url;
		return this.checkLogin(url);
	}

  checkLogin(url: string):boolean{
    if (this.authService.isLoggedIn) { return true; }
    this.authService.redirectUrl = url;
    this.router.navigate(['/login']);
    return false;

  }

}
