import { Component }   				from '@angular/core';
import { Router }      				from '@angular/router';
import { AuthService } 				from './auth.service';
import { Title } 							from '@angular/platform-browser';
// We import here the package of Notification message package.
import { Ng2IzitoastService } from 'ng2-izitoast';

@Component({
	template: `
    <div class='row'>
    <div class="col s12 m4 offset-m4">
    <div class="card hoverable">

      <div class="card-content">
          <span class="card-title">Connexion Page</span>
          <p><em>{{message}}</em></p>
      </div>

      <div class="card-action">
        <a (click)="login()"  *ngIf="!authService.isLoggedIn">Login</a>
        <a (click)="logout()" *ngIf="authService.isLoggedIn">Logout</a>
      </div>
    </div>
    </div>
    </div>
  `
})
export class LoginComponent {
	message: string;
	constructor(private authService: AuthService, private router: Router,
							private title: Title, private iziToast: Ng2IzitoastService ) {
		this.setMessage();
	}

	// Advise the user on the authentification.
	setMessage() {
		if (this.authService.isLoggedIn){
			this.message = 'You are login.' ;
			this.iziToast.success({
		    title: 'Login',
				position: 'topCenter',
				transitionIn: 'flipInX',
		    message: 'Successfully login!',
			});
			this.modifyTitle("Login");
		}else{
			this.message = 'You are logout.';
			this.iziToast.warning({
				title: 'Logout',
				position: 'topCenter',
				transitionIn: 'flipInX',
				message: 'You\'re logout of the application !',
			});
			this.modifyTitle("Logout");
		}
	}

	// Connect the user on the guard
	login() {
		this.message = 'Connection attempt in progress ...';

		this.authService.login().subscribe(() => {
			this.setMessage();
			if (this.authService.isLoggedIn) {
				// Recover the redirection url since the authentification service
				// if no one redirection has been defined, we redirect the user on the liste of pokemons.
				let redirect = this.authService.redirectUrl ? this.authService.redirectUrl : '/pokemon/all';
				// Redirect the user
				this.router.navigate([redirect]);
				this.modifyTitle("all Pokemons");
			}
		});
	}

	// Logout the user
	logout() {
		this.authService.logout();
		this.setMessage();
		this.modifyTitle("Logout");
	}

	public modifyTitle(newTitle: string) {
		this.title.setTitle(newTitle);
	}
}
