import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'pokemon-app',
  template: `
  <!-- navigation tab -->
  <nav>
      <div class="nav-wrapper teal">
        <a href="#" class="brand-logo center">pokemon-app</a>
      </div>
  </nav>

  <!-- Content of components -->
  <router-outlet></router-outlet>
  `

})
export class AppComponent {}
