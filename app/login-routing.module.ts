import { NgModule }       from '@angular/core';
import { RouterModule }   from '@angular/router';
import { AuthGuard }      from './auth-guard.service';
import { AuthService }    from './auth.service';
import { LoginComponent } from './login.component';
import { Title } from '@angular/platform-browser';

@NgModule({
  imports: [
    // the other module is for any imported modules.
    RouterModule.forChild([
      { path: 'login', component: LoginComponent }    ])
  ],
  exports: [
    RouterModule
  ],
  providers: [
    AuthGuard,
    AuthService,
    Title
  ]
})
export class LoginRoutingModule {}
