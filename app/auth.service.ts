import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthService {
  isLoggedIn: boolean = false; // The user is login ?
  redirectUrl: string; // Where redirect the user after authentification ?

  // login method
  login(): Observable<boolean> {
    return Observable.of(true).delay(1000).do(val => this.isLoggedIn = true);
  }

  // logout method
  logout(): void {
    this.isLoggedIn = false;
  }
}
