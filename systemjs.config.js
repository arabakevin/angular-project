/**
 * The configuration of SystemJS for our application.
 * We can modify this file later according to our needs.
 */
(function (global) {
  System.config({
    paths: {
      // Define a shortcut, 'npm' will point to 'node_modules'
      'npm:': 'node_modules/'
    },
    // The map option is used to tell SystemJS where to load items
    map: {
      // Us application find in the folder 'dist'
      app: 'dist',

      // packets angular
      '@angular/core': 'npm:@angular/core/bundles/core.umd.js',
      '@angular/common': 'npm:@angular/common/bundles/common.umd.js',
      '@angular/compiler': 'npm:@angular/compiler/bundles/compiler.umd.js',
      '@angular/platform-browser': 'npm:@angular/platform-browser/bundles/platform-browser.umd.js',
      '@angular/platform-browser-dynamic': 'npm:@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
      '@angular/http': 'npm:@angular/http/bundles/http.umd.js',
      '@angular/router': 'npm:@angular/router/bundles/router.umd.js',
      '@angular/forms': 'npm:@angular/forms/bundles/forms.umd.js',

      // Other librairies
      'rxjs':                      'npm:rxjs',
      // This one is a library web api to simulate a backend part
      'angular-in-memory-web-api': 'npm:angular-in-memory-web-api/bundles/in-memory-web-api.umd.js',
      // This one is a library who create a notification message system.
      'ng2-izitoast': 'npm:ng2-izitoast/bundles/ng2-izitoast.umd.js'
    },
    // The 'packages' option tells SystemJS how to load packages that do not have any files and / or extensions populated
    packages: {
      app: {
        main: './main.js',
        defaultExtension: 'js'
      },
      rxjs: {
        defaultExtension: 'js'
      }
    }
  });
})(this);
