# Little application with angular

__Introduction angular project__

*Summary application*

It's just an application that lists the skills of several pokemons.
We can say that it is a little customizable pokedex.

![](app/assets/screen_application.png)

__Create an angular project__

1 ==> Create a package.json. it allow to describe the dependancies of projet to Node Package Manager

```
{
  "name": "ng2-pokemon-app",
  "version": "1.0.0",
  "description": "A awesome app for managing pokemons, to train at Angular",
  "scripts": {
    "start": "tsc && concurrently \"tsc -w\" \"lite-server\" ",
    "e2e": "tsc && concurrently \"http-server -s\" \"protractor protractor.config.js\" --kill-others --success first",
    "lint": "tslint ./app/**/*.ts -t verbose",
    "lite": "lite-server",
    "pree2e": "webdriver-manager update",
    "test": "tsc && concurrently \"tsc -w\" \"karma start karma.conf.js\"",
    "test-once": "tsc && karma start karma.conf.js --single-run",
    "tsc": "tsc",
    "tsc:w": "tsc -w"
  },
  "keywords": [],
  "author": "",
  "license": "MIT",
  "dependencies": {
    "@angular/common": "~4.0.0",
    "@angular/compiler": "~4.0.0",
    "@angular/core": "~4.0.0",
    "@angular/forms": "~4.0.0",
    "@angular/http": "~4.0.0",
    "@angular/platform-browser": "~4.0.0",
    "@angular/platform-browser-dynamic": "~4.0.0",
    "@angular/router": "~4.0.0",

    "angular-in-memory-web-api": "~0.3.0",
    "systemjs": "0.19.40",
    "core-js": "^2.4.1",
    "rxjs": "5.0.1",
    "zone.js": "^0.8.4"
  },
  "devDependencies": {
    "concurrently": "^3.2.0",
    "lite-server": "^2.2.2",
    "typescript": "~2.1.5",

    "canonical-path": "0.0.2",
    "http-server": "^0.9.0",
    "tslint": "^3.15.1",
    "lodash": "^4.16.4",
    "jasmine-core": "~2.4.1",
    "karma": "^1.3.0",
    "karma-chrome-launcher": "^2.0.0",
    "karma-cli": "^1.0.1",
    "karma-jasmine": "^1.0.2",
    "karma-jasmine-html-reporter": "^0.2.2",
    "protractor": "~4.0.14",
    "rimraf": "^2.5.4",

    "@types/node": "^6.0.46",
    "@types/jasmine": "2.5.36"
  },
  "repository": {}
}
```

There are three interessants parts to remark in this file (package.json)

I. Les scripts : un certains nombre de scripts pré-définis sont listés à partir de la ligne 5.
 start permet de démarrer notre application
 lite  permet de démarrer un mini-serveur sur lequel tournera notre application Angular

II. Les dépendances : A partir de la ligne 19, apparaît une liste de toutes les dépendances de notre application
 - @angular/router  a pour vocation de gérer les routes de notre formulaire
 - @angular/forms les formulaires.

III. Les dépendances relatives au développement : Ces dépendances sont listées à partir de la ligne 35, et concernent les dépendances dont nous n'aurons plus besoin quand notre application sera terminée.

2 ==> Create a file System.js

```
/**
 * The setting of SystemJS for us application.
 * We can modify this file according to our needs.
 */
(function (global) {
  System.config({
    paths: {
      // define a shortcut, 'npm' go to 'node_modules'
      'npm:': 'node_modules/'
    },
    // The option map allow indicate in SystemJS the directory/way of elements
    map: {
      // Us application find in the folder 'dist'
      app: 'dist',

      // packets angular
      '@angular/core': 'npm:@angular/core/bundles/core.umd.js',
      '@angular/common': 'npm:@angular/common/bundles/common.umd.js',
      '@angular/compiler': 'npm:@angular/compiler/bundles/compiler.umd.js',
      '@angular/platform-browser': 'npm:@angular/platform-browser/bundles/platform-browser.umd.js',
      '@angular/platform-browser-dynamic': 'npm:@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
      '@angular/http': 'npm:@angular/http/bundles/http.umd.js',
      '@angular/router': 'npm:@angular/router/bundles/router.umd.js',
      '@angular/forms': 'npm:@angular/forms/bundles/forms.umd.js',

      // other librairies
      'rxjs':                      'npm:rxjs',
      'angular-in-memory-web-api': 'npm:angular-in-memory-web-api/bundles/in-memory-web-api.umd.js'
    },
    // The option 'packages' indicate in SystemJS How charge the package who don't have file or extensions knowledgeable
    packages: {
      app: {
        main: './main.js',
        defaultExtension: 'js'
      },
      rxjs: {
        defaultExtension: 'js'
      }
    }
  });
})(this);
```

3 ==> Create a file tsconfig.json

- line 3 target has for value es5,
A la ligne 3, target a pour valeur es5, which indicates that our TypeScript code will be compiled to the ES5 JavaScript code.

- line 12, removeComments tells TypeScript that we do not want comments from our code removed during compilation.

```
{
  "compilerOptions": {
    "target": "es5",
    "module": "commonjs",
    "moduleResolution": "node",
    "sourceMap": true,
    "emitDecoratorMetadata": true,
    "experimentalDecorators": true,
    "lib": [ "es2015", "dom" ],
    "noImplicitAny": true,
    "suppressImplicitAnyIndexErrors": true,
    "removeComments": false,
    "outDir": "dist"
  }
}
```

4 ==> Install all dependancies

```
npm install
```

5 ==> Create a folder app

6 ==> We create a file app.component.ts inside app folder
In this file in fact we are creating us first component.

```
import { Component } from '@angular/core';

@Component({
	selector: 'pokemon-app',
	template: '<h1>Hello, Angular 2 !</h1>',
})
export class AppComponent { }
```

- line 1, we import the annotation Component since the core of Angular: @angular/core.
- line 3, we remark @component it's allow us to define a component.

To define a component @component must have in minimum two values
	- selector represent in one word the name of component
	- template it's the HTML code of component

However in line 7, we can see the code of class who belong in us component.

It is this class that will contain the logic of our component. The export keyword makes the component accessible to other files.


MODULE serves to group the different components of the same kind / family between them (eg all the components used for the blog, ...)

At a minimum, your application must contain a module: the root module.

That's why it's necessary to create one in our application

in the app folder we will have an app.module.ts file.

```
import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent }  from './app.component';

@NgModule({
  imports:      [ BrowserModule ],
  declarations: [ AppComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
```

1. imports: allows to declare all the elements that one needs to import in our module. The root modules need to import the BrowserModule (unlike other modules that we will add later in our application).

2. declarations: A list of all the components and directives that belong to this module. So we added our unique AppComponent component.
BrowserModule, which is a module that provides essential elements for the operation of the application, like the ngIf and ngFor directives in all our templates

3. bootstrap: Identifies the root component that Angular calls when the application starts. Since the root module is started automatically by Angular when the application starts, and AppComponent is the root component of the root module, AppComponent will appear when the application starts.

7 ==> Create a file main.ts

```
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app.module';

platformBrowserDynamic().bootstrapModule(AppModule);
```

And in the root folder create an index.html file

```
<!DOCTYPE html>
<html>
  <head>
    <title>Angular QuickStart</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Load bookstores -->
    <!-- Polyfill(s) for old browsers -->
    <script src="node_modules/core-js/client/shim.min.js"></script>

    <script src="node_modules/zone.js/dist/zone.js"></script>
    <script src="node_modules/systemjs/dist/system.src.js"></script>
    <!-- 2. Configuration de SystemJS -->
    <script src="systemjs.config.js"></script>
    <script>
      System.import('app').catch(function(err){ console.error(err); });
    </script>
  </head>
  <!-- 3. Display l'application -->
  <body>
    <pokemon-app>Loading AppComponent content here ...</pokemon-app>
  </body>
</html>
```

__Install and Start the angular project__

```
npm install && npm start
```

To do the housework
- Create the remote folder and insert all the * .js and * .js.map files that are currently in the app folder.

and restart the server
